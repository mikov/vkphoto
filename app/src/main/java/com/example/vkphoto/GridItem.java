package com.example.vkphoto;

public class GridItem {
    private String bigSize;
    private String smallSize;
    private String title;

    GridItem(String smallSize, String bigSize, String title){
        this.smallSize = smallSize;
        this.bigSize = bigSize;
        this.title = title;
    }
    public String getBigSize() {
        return bigSize;
    }
    public String getSmallSize(){
        return smallSize;
    }
    public String getTitle(){
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public void setBigSize(String bigSize) {
        this.bigSize = bigSize;
    }
    public void setSmallSize(String smallSize) {
        this.smallSize = smallSize;
    }
}
