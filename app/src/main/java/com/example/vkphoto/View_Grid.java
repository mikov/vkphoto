package com.example.vkphoto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

public class View_Grid extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__grid);

        final ImageContainer container = new ImageContainer(
                (ArrayList<String>)getIntent().getExtras().get("ARRAY_OF_SMALL_SIZES"),
                (ArrayList<String>)getIntent().getExtras().get("ARRAY_OF_BIG_SIZES"),
                (ArrayList<String>)getIntent().getExtras().get("ARRAY_OF_TITLES"));



        GridView gridview = findViewById(R.id.gridView);
        gridview.setAdapter(new ImageAdapter(this, container));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent intent = new Intent(View_Grid.this,FullScreenView.class);
                intent.putExtra("PHOTOS", container.getArrayOfBigSize());
                intent.putExtra("TITLES", container.getArrayOfTitle());
                intent.putExtra("POSITION", position);
                startActivity(intent);
            }
        });

    }


}
