package com.example.vkphoto;

import java.util.ArrayList;

public class ImageContainer {
    private ArrayList<GridItem> imageArray;

    ImageContainer(){
        imageArray = new ArrayList<>();
    }

    ImageContainer(ImageContainer img){
        if (this != img) {
            this.imageArray = img.imageArray;
        }
    }

    ImageContainer(ArrayList<String>small, ArrayList<String> big, ArrayList<String> title){
        int size = small.size();
        imageArray = new ArrayList<>();
        for (int i = 0; i < size; ++i){
            imageArray.add(new GridItem(small.get(i),big.get(i),title.get(i)));
        }
    }

    public void add(GridItem item){
        imageArray.add(item);
    }

    public void add(String small, String big, String text) {
        GridItem item = new GridItem(small,big,text);
        imageArray.add(item);
    }

    public int length(){
        return imageArray.size();
    }

    public GridItem getItem(int i){
        return imageArray.get(i);
    }

    public ArrayList<String> getArrayOfSmallSize(){
        ArrayList<String> list = new ArrayList<>();
        int size = imageArray.size();
        for (int i = 0; i < size; ++i){
            list.add(this.imageArray.get(i).getSmallSize());
        }
        return list;
    }
    public ArrayList<String> getArrayOfBigSize(){
        ArrayList<String> list = new ArrayList<>();
        int size = imageArray.size();
        for (int i = 0; i < size; ++i){
            list.add(this.imageArray.get(i).getBigSize());
        }
        return list;
    }
    public ArrayList<String> getArrayOfTitle(){
        ArrayList<String> list = new ArrayList<>();
        int size = imageArray.size();
        for (int i = 0; i < size; ++i){
            list.add(this.imageArray.get(i).getTitle());
        }
        return list;
    }
}
